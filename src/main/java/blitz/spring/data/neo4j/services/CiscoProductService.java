package blitz.spring.data.neo4j.services;

import blitz.spring.data.neo4j.domain.CiscoProduct;
import blitz.spring.data.neo4j.repositories.CiscoProductRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

@Service
public class CiscoProductService {

    private final static Logger LOG = LoggerFactory.getLogger(MovieService.class);

    private final CiscoProductRepository ciscoProductRepository;
    public CiscoProductService(CiscoProductRepository ciscoProductRepository) {
        this.ciscoProductRepository = ciscoProductRepository;
    }

    @Transactional(readOnly = true)
    public Collection<CiscoProduct> findByName(String name) {
        return ciscoProductRepository.product(name);
    }

    @Transactional(readOnly = true)
    public Collection<CiscoProduct> matchesHeight(int height) {
        return ciscoProductRepository.productHeight(height);
    }
}
