package blitz.spring.data.neo4j.services;

import com.clivern.wit.api.Message;
import com.clivern.wit.api.endpoint.MessageEndpoint;
import com.clivern.wit.util.Config;
import com.clivern.wit.Wit;
import org.springframework.stereotype.Service;

// Wit.ai NLP Client.  Wit.ai will return a set of Intent & Slot mapping for the query that we pass.
// We need to train the models, in their UI. Contact  ravikiran, to know the details on what sample queries that we are training.
@Service
public class WitNLPClient {
    private Wit witClient;
    public WitNLPClient() {

        // Init Config.
        Config config = new Config();
        config.set("wit_api_id", "2011233938920514");
        config.set("wit_access_token", "A6K4ULXLPYWNZING2XKLH5A63W2AROIN");
        config.set("logging_level", "debug");
        config.set("logging_file_path", "/src/main/java/resources/");
        config.set("logging_file_format", "current_date");
        config.set("logging_log_type", "both");
        config.set("logging_current_date_format", "yyyy-MM-dd");
        config.set("logging_append", "true");
        config.set("logging_buffered", "true");
        config.configLogger();

        witClient = new Wit(config);

    }

    public String query(String query) {
        Message message = new Message(MessageEndpoint.GET);
        message.setQ(query);
        //message.setContext("");
        //message.setMsgId("789");
        //message.setThreadId("fb_th");
        //message.setN(6);
        message.setVerbose(true);
        String result = "";
        String error = "";

        try {
            if (witClient.send(message)) {
                result = witClient.getResponse();
                System.out.println("RAVIKIRAN Result from Wit is " + result);
                return result;
            } else {
                error = witClient.getError();
                System.out.println("RAVIKIRAN error from wit is " + error);
                return  error;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
