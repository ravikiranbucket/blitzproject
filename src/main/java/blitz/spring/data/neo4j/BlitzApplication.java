package blitz.spring.data.neo4j;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.neo4j.repository.config.EnableNeo4jRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableNeo4jRepositories("blitz.spring.data.neo4j.repositories")
public class BlitzApplication {

    public static void main(String[] args) {
        SpringApplication.run(BlitzApplication.class, args);
    }
}