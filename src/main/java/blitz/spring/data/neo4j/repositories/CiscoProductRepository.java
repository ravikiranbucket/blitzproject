package blitz.spring.data.neo4j.repositories;

import blitz.spring.data.neo4j.domain.CiscoProduct;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.Collection;

@RepositoryRestResource(collectionResourceRel = "cisco_product", path = "cisco_product")
public interface CiscoProductRepository extends Neo4jRepository<CiscoProduct, Long> {

    @Query("MATCH (cp:CiscoProduct) WHERE cp.name contains {name}  RETURN cp;")
    Collection<CiscoProduct> product(@Param("name") String name);

    @Query("MATCH (cp:CiscoProduct)-[r]->(d:Dimension) WHERE d.height > {height} RETURN  cp;")
    Collection<CiscoProduct> productHeight(@Param("height") int height);

}
