package blitz.spring.data.neo4j.controller;

import blitz.spring.data.neo4j.services.WitNLPClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class NLPController {
    private WitNLPClient witNLPClient;
    public NLPController(WitNLPClient witNLPClient) {
        this.witNLPClient = witNLPClient;
    }

    @GetMapping("/resolve")
    public String nluEntity(@RequestParam(value = "query",required = true) String query) {
        return witNLPClient.query(query);
    }
}
