package blitz.spring.data.neo4j.controller;

import blitz.spring.data.neo4j.domain.CiscoProduct;
import blitz.spring.data.neo4j.services.CiscoProductService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RestController
@RequestMapping("/")
public class CiscoProductController {

    private CiscoProductService ciscoProductService;
    public CiscoProductController(CiscoProductService ciscoProductService) {
        this.ciscoProductService = ciscoProductService;
    }

    @GetMapping("/cisco_product")
    public Collection<CiscoProduct> ciscoProduct(
            @RequestParam(value = "name",required = false) String name,
            @RequestParam(value = "height",required = false) Integer height) {
        if (StringUtils.isNotBlank(name)) {
            return ciscoProductService.findByName(name);
        } else if (height != null) {
            return ciscoProductService.matchesHeight(height);
        }
        return null;
    }
}
