package blitz.spring.data.neo4j.controller;

import java.util.Collection;
import java.util.Map;

import blitz.spring.data.neo4j.domain.Movie;
import blitz.spring.data.neo4j.services.MovieService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class MovieController {

	private final MovieService movieService;
	
	public MovieController(MovieService movieService) {
		this.movieService = movieService;
	}

    @GetMapping("/graph")
	public Map<String, Object> graph(@RequestParam(value = "limit",required = false) Integer limit) {
		return movieService.graph(limit == null ? 100 : limit);
	}

    @GetMapping("/movieList")
    public Collection<Movie> findTitleLike(@RequestParam(value = "title", required = true) String title) {
        return movieService.findByTitleLike(title);
    }

	@GetMapping("/movie")
	public Object findTitle(@RequestParam(value = "title", required = true) String title) {
		return movieService.findByTitle(title);
	}

}
