# Overview

This directory hosts all the design diagrams, in a dot-code format ( for easy maintainability ).

To render this, please go to [Plant UML Online editor](https://www.planttext.com/) and paste the code mentioned in the .pu file.

There are also lots of plugins available for various IDE's, which can render the dot code right in your IDE. Please search for Plant UML <Your IDE> plugin.


