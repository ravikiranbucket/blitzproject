## Dev Getting Started Guide ##


### Step1: Install Neo4j ###

1. Download and install [Neo4J on your Desktop](https://neo4j.com/download/)
2. Make sure you remember the password, when you set it up.  We need to add it to the properties file in the project otherwise your application won't run.
3. Start Neo4J browser, from your Desktop app.
4. Log in, with the password.


### Step2: Add content to Neo4j DB ###

1. Install the Movies dataset with `:play movies`
2. Click the large CREATE statement and hit the triangular “Run” button to insert the data.
3. Also paste the following query ( This is creating a sample graph for 2 Cisco products . This query is a sample and DOES NOT reflect how our end query will look like.

```java

CREATE (ncs4201_dimension:Dimension{ len: 1.72, width: 17.5, height: 10, unit: "in"})
CREATE (ncs4201_weight:Weight{ value: 8.5, unit: "lb"})
CREATE (rackmountetsi:Rack{value : "ETSI rack-mount"})
CREATE (rackmount19:Rack{value : "19-in rack-mount"})
CREATE (rackmount23:Rack{value : "23-in rack-mount"})
CREATE (cisconcs4201:CiscoProduct{ productId:"CISCONCS4201", name:"Cisco NCS 4201-SA"})
CREATE (cisconcs4201)-[:SYSTEM_SPEC { type: 'physical_spec'}]->(ncs4201_dimension),
(cisconcs4201)-[:SYSTEM_SPEC { type: 'weight'}]->(ncs4201_weight),
(cisconcs4201)-[:SYSTEM_SPEC { type: 'rack_mount'}]->(rackmountetsi),
(cisconcs4201)-[:SYSTEM_SPEC { type: 'rack_mount'}]->(rackmount19),
(cisconcs4201)-[:SYSTEM_SPEC { type: 'rack_mount'}]->(rackmount23)
CREATE (ncs4201_powerconsump:Power{ max: 145, typical: 110, unit: "Watt"})
CREATE (ncs4201_acinput_voltage_range:Voltage{ low: 85, high: 264, unit: "Volt AC"})
CREATE (ncs4201_acinput_voltage_nominal:Voltage{ low: 100, high: 240, unit: "Volt AC"})
CREATE (ncs4201_dcinput_voltage_range1:Voltage{ low : -32, high: -18, unit: "Volt DC"})
CREATE (ncs4201_dcinput_voltage_range2:Voltage{ low : -72, high: -36, unit: "Volt DC"})
CREATE (ncs4201_dcinput_voltage_nominal:Voltage{ low : -48, high: -24, unit: "Volt DC"})
CREATE (ncs4201_acinput_frequency_range:Frequency{low: 47, high: 63, unit: "Hz"})
CREATE (ncs4201_acinput_frequency_nominal:Frequency{low: 50, high: 60, unit: "Hz"})
CREATE (cisconcs4201)-[:POWER_SPEC { type: 'power_consumption'}]->(ncs4201_powerconsump),
(cisconcs4201)-[:POWER_SPEC { type: 'ac_voltage', subType: 'range'}]->(ncs4201_acinput_voltage_range),
(cisconcs4201)-[:POWER_SPEC { type: 'ac_voltage', subType: 'nominal'}]->(ncs4201_acinput_voltage_nominal),
(cisconcs4201)-[:POWER_SPEC { type: 'ac_frequency', subType: 'range'}]->(ncs4201_acinput_frequency_range),
(cisconcs4201)-[:POWER_SPEC { type: 'ac_frequency', subType: 'nominal'}]->(ncs4201_acinput_frequency_nominal),
(cisconcs4201)-[:POWER_SPEC { type: 'dc_voltage', subType: 'range'}]->(ncs4201_dcinput_voltage_range1),
(cisconcs4201)-[:POWER_SPEC { type: 'dc_voltage', subType: 'range'}]->(ncs4201_dcinput_voltage_range2),
(cisconcs4201)-[:POWER_SPEC { type: 'dc_voltage', subType: 'nominal'}]->(ncs4201_dcinput_voltage_nominal)
CREATE (ncs4202_dimension:Dimension{ len: 1.73, width: 17.5, height: 11.28, unit: "in"})
CREATE (ncs4202_weight:Weight{ value: 9.25, unit: "lb"})
CREATE (cisconcs4202:CiscoProduct{ productId:"CISCONCS4202", name:"Cisco NCS 4202-SA"})
CREATE (cisconcs4202)-[:SYSTEM_SPEC { type: 'physical_spec'}]->(ncs4202_dimension),
(cisconcs4202)-[:SYSTEM_SPEC { type: 'weight'}]->(ncs4202_weight),
(cisconcs4202)-[:SYSTEM_SPEC { type: 'rack_mount'}]->(rackmountetsi),
(cisconcs4202)-[:SYSTEM_SPEC { type: 'rack_mount'}]->(rackmount19),
(cisconcs4202)-[:SYSTEM_SPEC { type: 'rack_mount'}]->(rackmount23)
CREATE (ncs4202_powerconsump:Power{ max: 150, typical: 130, unit: "Watt"})
CREATE (ncs4202_acinput_voltage_range:Voltage{ low: 85, high: 264, unit: "Volt AC"})
CREATE (ncs4202_acinput_voltage_nominal:Voltage{ low: 100, high: 240, unit: "Volt AC"})
CREATE (ncs4202_dcinput_voltage_range1:Voltage{ low : -32, high: -18, unit: "Volt DC"})
CREATE (ncs4202_dcinput_voltage_range2:Voltage{ low : -72, high: -40, unit: "Volt DC"})
CREATE (ncs4202_dcinput_voltage_nominal:Voltage{ low : -60, high: -24, unit: "Volt DC"})
CREATE (ncs4202_acinput_frequency_range:Frequency{low: 47, high: 63, unit: "Hz"})
CREATE (ncs4202_acinput_frequency_nominal:Frequency{low: 50, high: 60, unit: "Hz"})
CREATE (cisconcs4202)-[:POWER_SPEC { type: 'power_consumption'}]->(ncs4202_powerconsump),
(cisconcs4202)-[:POWER_SPEC { type: 'ac_voltage', subType: 'range'}]->(ncs4202_acinput_voltage_range),
(cisconcs4202)-[:POWER_SPEC { type: 'ac_voltage', subType: 'nominal'}]->(ncs4202_acinput_voltage_nominal),
(cisconcs4202)-[:POWER_SPEC { type: 'ac_frequency', subType: 'range'}]->(ncs4202_acinput_frequency_range),
(cisconcs4202)-[:POWER_SPEC { type: 'ac_frequency', subType: 'nominal'}]->(ncs4202_acinput_frequency_nominal),
(cisconcs4202)-[:POWER_SPEC { type: 'dc_voltage', subType: 'range'}]->(ncs4202_dcinput_voltage_range1),
(cisconcs4202)-[:POWER_SPEC { type: 'dc_voltage', subType: 'range'}]->(ncs4202_dcinput_voltage_range2),
(cisconcs4202)-[:POWER_SPEC { type: 'dc_voltage', subType: 'nominal'}]->(ncs4202_dcinput_voltage_nominal)

```

### Step3: Running our application ###

1. Clone this project from GitHub
2. Update `src/main/resources/application.properties` with the username and password you set above
3. Run the project with `mvn spring-boot:run`


### Step4: Testing your service ###

1. On a separate terminal, execute the following queries
  1. `curl "http://localhost:8080/cisco_product?height=10" `  -> Should return you a Cisco product.
  2. `curl "http://localhost:8080/cisco_product?name=4202"`   -> Should return you a Cisco product.
  3. `curl "http://localhost:8080/movie?title=The%20Matrix"`  -> Should return you info on Matrix movie.
  4. `curl "http://localhost:8080/resolve?query=min%20temperature%20supported"` -> should return you a list of entities.
What we've demonstrated here is that our service can host multiple endpoints, each trying to do totally different things.


## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).
